/**********************************************************************************
*                             MAIN QUADRUPOLES
***********************************************************************************/

kbrqf              =         0.7330236081 ;
kbrqd              =        -0.7463980996 ;

/**********************************************************************************
*                             INJECTION BUMP
***********************************************************************************/

bsw_k0l            =         0.0000000000 ;
bsw_k2l            =         0.0000000000 ;

!-------------------------------------------------
! Ring 1
!-------------------------------------------------

kbi1ksw1l4         =         0.0000000000 ;
kbi1ksw2l1         =         0.0000000000 ;
kbi1ksw16l1        =         0.0000000000 ;
kbi1ksw16l4        =         0.0000000000 ;

k0bi1bsw1l11       =         0.0000000000 ;
k0bi1bsw1l12       =         0.0000000000 ;
k0bi1bsw1l13       =         0.0000000000 ;
k0bi1bsw1l14       =         0.0000000000 ;


!-------------------------------------------------
! Ring 2
!-------------------------------------------------

kbi2ksw1l4         =         0.0000000000 ;
kbi2ksw2l1         =         0.0000000000 ;
kbi2ksw16l1        =         0.0000000000 ;
kbi2ksw16l4        =         0.0000000000 ;

k0bi2bsw1l11       =         0.0000000000 ;
k0bi2bsw1l12       =         0.0000000000 ;
k0bi2bsw1l13       =         0.0000000000 ;
k0bi2bsw1l14       =         0.0000000000 ;


!-------------------------------------------------
! Ring 3
!-------------------------------------------------

kbi3ksw1l4         =         0.0000000000 ;
kbi3ksw2l1         =         0.0000000000 ;
kbi3ksw16l1        =         0.0000000000 ;
kbi3ksw16l4        =         0.0000000000 ;

k0bi3bsw1l11       =         0.0000000000 ;
k0bi3bsw1l12       =         0.0000000000 ;
k0bi3bsw1l13       =         0.0000000000 ;
k0bi3bsw1l14       =         0.0000000000 ;


!-------------------------------------------------
! Ring 4
!-------------------------------------------------

kbi4ksw1l4         =         0.0000000000 ;
kbi4ksw2l1         =         0.0000000000 ;
kbi4ksw16l1        =         0.0000000000 ;
kbi4ksw16l4        =         0.0000000000 ;

k0bi4bsw1l11       =         0.0000000000 ;
k0bi4bsw1l12       =         0.0000000000 ;
k0bi4bsw1l13       =         0.0000000000 ;
k0bi4bsw1l14       =         0.0000000000 ;

/**********************************************************************************
*                             BETA-BEATING CORRECTION
***********************************************************************************/

kbrqd3corr         =         0.0000000000 ;
kbrqd14corr        =         0.0000000000 ;

/**********************************************************************************
*                             EXTRACTION BUMP
***********************************************************************************/

kbebsw14l4         =        -0.0046940361 ;
kbebsw15l1         =         0.0064090842 ;
kbebsw15l4         =        -0.0047985246 ;

/**********************************************************************************
*                                   KNOBS
***********************************************************************************/

!-------------------------------------------------
! Common to all rings
!-------------------------------------------------

! Extraction bump knob using the absoute position

bebsw_x_mm         =         -16.7000000000 ;

dkbebsw14L4_x      =         0.0002810800 ;
dkbebsw15L1_x      =        -0.0003837775 ;
dkbebsw15L4_x      =         0.0002873368 ;

kbebsw14l4 := dkbebsw14L4_x * bebsw_x_mm;
kbebsw15l1 := dkbebsw15L1_x * bebsw_x_mm;
kbebsw15l4 := dkbebsw15L4_x * bebsw_x_mm;

!-------------------------------------------------
! Ring 1
!-------------------------------------------------

! Extraction bump correction

be1dhz_x_mm        =         0.0000000000 ;
be1dvt_y_mm        =         0.0000000000 ;
be1dhz_px_urad     =         0.0000000000 ;
be1dvt_py_urad     =         0.0000000000 ;

dkbe1dhz4l1_x      =        -0.0000304652 ;
dkbe1dhz11l1_x     =         0.0002162683 ;
dkbe1dhz4l1_px     =         0.0000012924 ;
dkbe1dhz11l1_px    =        -0.0000004286 ;

dkbe1dvt4l1_y      =        -0.0000428650 ;
dkbe1dvt11l1_y     =         0.0003230362 ;
dkbe1dvt4l1_py     =         0.0000014895 ;
dkbe1dvt11l1_py    =        -0.0000005847 ;

kbe1dhz4l1   := dkbe1dhz4l1_x  * be1dhz_x_mm + dkbe1dhz4l1_px  * be1dhz_px_urad ;
kbe1dhz11l1  := dkbe1dhz11l1_x * be1dhz_x_mm + dkbe1dhz11l1_px * be1dhz_px_urad ;
kbe1dvt4l1   := dkbe1dvt4l1_y  * be1dvt_y_mm + dkbe1dvt4l1_py  * be1dvt_py_urad ;
kbe1dvt11l1  := dkbe1dvt11l1_y * be1dvt_y_mm + dkbe1dvt11l1_py * be1dvt_py_urad ;

!-------------------------------------------------
! Ring 2
!-------------------------------------------------

! Extraction bump correction

be2dhz_x_mm        =         0.0000000000 ;
be2dvt_y_mm        =         0.0000000000 ;
be2dhz_px_urad     =         0.0000000000 ;
be2dvt_py_urad     =         0.0000000000 ;

dkbe2dhz4l1_x      =        -0.0000304652 ;
dkbe2dhz11l1_x     =         0.0002162683 ;
dkbe2dhz4l1_px     =         0.0000012924 ;
dkbe2dhz11l1_px    =        -0.0000004286 ;

dkbe2dvt4l1_y      =        -0.0000428650 ;
dkbe2dvt11l1_y     =         0.0003230362 ;
dkbe2dvt4l1_py     =         0.0000014895 ;
dkbe2dvt11l1_py    =        -0.0000005847 ;

kbe2dhz4l1   := dkbe2dhz4l1_x  * be2dhz_x_mm + dkbe2dhz4l1_px  * be2dhz_px_urad ;
kbe2dhz11l1  := dkbe2dhz11l1_x * be2dhz_x_mm + dkbe2dhz11l1_px * be2dhz_px_urad ;
kbe2dvt4l1   := dkbe2dvt4l1_y  * be2dvt_y_mm + dkbe2dvt4l1_py  * be2dvt_py_urad ;
kbe2dvt11l1  := dkbe2dvt11l1_y * be2dvt_y_mm + dkbe2dvt11l1_py * be2dvt_py_urad ;

!-------------------------------------------------
! Ring 3
!-------------------------------------------------

! Extraction bump correction

be3dhz_x_mm        =         0.0000000000 ;
be3dvt_y_mm        =         0.0000000000 ;
be3dhz_px_urad     =         0.0000000000 ;
be3dvt_py_urad     =         0.0000000000 ;

dkbe3dhz4l1_x      =        -0.0000304652 ;
dkbe3dhz11l1_x     =         0.0002162683 ;
dkbe3dhz4l1_px     =         0.0000012924 ;
dkbe3dhz11l1_px    =        -0.0000004286 ;

dkbe3dvt4l1_y      =        -0.0000428650 ;
dkbe3dvt11l1_y     =         0.0003230362 ;
dkbe3dvt4l1_py     =         0.0000014895 ;
dkbe3dvt11l1_py    =        -0.0000005847 ;

kbe3dhz4l1   := dkbe3dhz4l1_x  * be3dhz_x_mm + dkbe3dhz4l1_px  * be3dhz_px_urad ;
kbe3dhz11l1  := dkbe3dhz11l1_x * be3dhz_x_mm + dkbe3dhz11l1_px * be3dhz_px_urad ;
kbe3dvt4l1   := dkbe3dvt4l1_y  * be3dvt_y_mm + dkbe3dvt4l1_py  * be3dvt_py_urad ;
kbe3dvt11l1  := dkbe3dvt11l1_y * be3dvt_y_mm + dkbe3dvt11l1_py * be3dvt_py_urad ;

!-------------------------------------------------
! Ring 4
!-------------------------------------------------

! Extraction bump correction

be4dhz_x_mm        =         0.0000000000 ;
be4dvt_y_mm        =         0.0000000000 ;
be4dhz_px_urad     =         0.0000000000 ;
be4dvt_py_urad     =         0.0000000000 ;

dkbe4dhz4l1_x      =        -0.0000304652 ;
dkbe4dhz11l1_x     =         0.0002162683 ;
dkbe4dhz4l1_px     =         0.0000012924 ;
dkbe4dhz11l1_px    =        -0.0000004286 ;

dkbe4dvt4l1_y      =        -0.0000428650 ;
dkbe4dvt11l1_y     =         0.0003230362 ;
dkbe4dvt4l1_py     =         0.0000014895 ;
dkbe4dvt11l1_py    =        -0.0000005847 ;

kbe4dhz4l1   := dkbe4dhz4l1_x  * be4dhz_x_mm + dkbe4dhz4l1_px  * be4dhz_px_urad ;
kbe4dhz11l1  := dkbe4dhz11l1_x * be4dhz_x_mm + dkbe4dhz11l1_px * be4dhz_px_urad ;
kbe4dvt4l1   := dkbe4dvt4l1_y  * be4dvt_y_mm + dkbe4dvt4l1_py  * be4dvt_py_urad ;
kbe4dvt11l1  := dkbe4dvt11l1_y * be4dvt_y_mm + dkbe4dvt11l1_py * be4dvt_py_urad ;
