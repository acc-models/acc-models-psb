system, "mkdir output";

/******************************************************************************************
 *
 * MAD-X input script for the flat top optics of the AD cycle.
 *
 * 09/10/2020 - Fanouria Antoniou, Hannes Bartosik, Chiara Bracco, 
 * Gian Piero di Giovanni, Matthew Fraser, Alexander Huschauer, Elisabeth Renner
 ******************************************************************************************/

 /******************************************************************
 * Energy and particle type definition
 ******************************************************************/

call, file="psb_ext_ad.beam";
BRHO      := BEAM->PC * 3.3356;

set,  format="20.10f";

/******************************************************************
 * Call lattice files
 ******************************************************************/

call, file = 'psb.seq';
call, file = 'psb_aperture.dbx';
call, file = 'psb_ext_ad.str';
call, file = 'macros.madx';

/******************************************************************
 * Tune matching 
 ******************************************************************/

Qx = 4.20;
Qy = 4.23;

use, sequence=psb1;
exec, match_tunes(Qx, Qy);

MATCH, USE_MACRO;
    VARY, NAME = kBRQF, STEP = 1e-6;
    VARY, NAME = kBRQD, STEP = 1e-6;
    USE_MACRO, name=ptc_twiss_macro(2,0,0);
    CONSTRAINT, EXPR = Table(ptc_twiss, PSB1$END, MU1) = Qx;
    CONSTRAINT, EXPR = Table(ptc_twiss, PSB1$END, MU2) = Qy;
    jacobian, calls=50000, bisec=3, tolerance=1e-15;
ENDMATCH; 

/******************************************************************
* Extraction bump
******************************************************************/

!exec, match_bebsw_bump(0., 0., 0, 0);

!exec, bebsw_knob_factors();

exec, absolute_bebsw_bump_knob();

exec, bedhzdvt_knob_factors();

exec, write_str_file("./output/psb_ext_ad.str");

/******************************************************************
 * PTC Twiss
 ******************************************************************/ 

use, sequence=psb1;
exec, ptc_twiss_macro(2,0,1);
exec, write_ptc_twiss("./output/psb_ext_ad.tfs");
